<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pos extends Model
{
  use SoftDeletes;

  protected $table = 'pos';
  protected $keyType = 'string';
  protected $guarded = [];

  public function user()
  {
      return $this->belongsTo('App\Model\User');
  }

  public function transactions()
  {
      return $this->hasMany('App\Model\Transaction');
  }
}