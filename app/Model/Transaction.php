<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $keyType = 'string';

    public function pos()
    {
        return $this->belongsTo('App\Model\Pos');
    }
}