<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
  
  protected $keyType = 'string';

  public function account()
  {
    return $this->belongsTo('App\Model\Account');
  }

  public function products()
  {
      return $this->hasMany('App\Model\Product');
  }

  public function pos()
  {
      return $this->hasMany('App\Model\Pos');
  }
}