<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    use SoftDeletes;

    protected $keyType = 'string';

    public function user()
    {
        return $this->hasOne('App\Model\User');
    }
}