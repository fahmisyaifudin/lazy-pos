<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Transaction;
use Illuminate\Database\Eloquent\Builder;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function index(){
        $user = Auth::user()->user;
        $trx = Transaction::whereHas('pos', function(Builder $query) use ($user) {
            $query->where('user_id', $user->id);
        })->with('pos')->get();
        return $this->successResponse($trx);
    }

    //
}
