<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Account;
use App\Model\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

     /**
       * @OA\Post(
            * path="/api/login",
            * tags={"Auth"},
       
            * @OA\RequestBody(
            *    @OA\MediaType(
            *       mediaType="application/x-www-form-urlencoded",
            *       @OA\Schema(
            *         required={"email","password"},
            *         @OA\Property(property="email", type="string", example="user1@mail.com"),
            *         @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
            *       )
            *    ),
            * ),
            * @OA\Response(
            *    response=200,
            *    description="Success",
            *    )
            *   )
          *)
       */

    public function login(Request $request)
    {
          //validate incoming request 
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
       * @OA\Post(
            * path="/api/register",
            * tags={"Auth"},
       
            * @OA\RequestBody(
            *    required=true,
            *    @OA\MediaType(
            *       mediaType="application/x-www-form-urlencoded",
            *       @OA\Schema(
            *           required={"email","password", "password_confirmation", "firstName", "shopName", "phone"},
            *           @OA\Property(property="email", type="string", format="password", example="user1@mail.com"),
            *           @OA\Property(property="password", type="string", format="password", example="PassWord12345"),
            *           @OA\Property(property="password_confirmation", type="string", format="password", example="PassWord12345"),
            *           @OA\Property(property="firstName", type="string", format="string", example="John"),
            *           @OA\Property(property="lastName", type="string", format="string", example="Doe"),
            *           @OA\Property(property="shopName", type="string", format="string", example="Cat Shop"),
            *           @OA\Property(property="phone", type="string", format="string", example="0865656576"),
            *       )
            *    ),
            * ),
             * @OA\Response(
                  *    response=200,
                  *    description="Success",
                  *    )
              *     )
          *)
       */

    public function register(Request $request)
    {
      $this->validate($request, [
        'email' => 'required|email|unique:accounts',
        'password' => 'required|confirmed',
        'firstName' => 'required',
        'shopName' => 'required',
        'phone' => 'required'
      ]);

      try {
        $account = new Account();
        $account->email = $request->input('email');
        $account->phone = $request->input('phone');
        $plainPassword = $request->input('password');
        $account->password = app('hash')->make($plainPassword);
        $account->save();

        $user = new User();
        $user->account_id = $account->id;
        $user->firstName = $request->input('firstName');
        $user->lastName = $request->input('lastName');
        $user->shopName = $request->input('shopName');
        $user->save();

        return response()->json(['message' => 'success', 'data' => [ 'account' => $account, 'user' => $user]]);
      }catch(\Exception $e){
        return response()->json(['message' => $e], 500);
      }
    }

    //
}
