<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Model\Product;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }


     /**
       * @OA\Post(
            * path="/api/product",
            * tags={"Product"},
            * security={ {"bearer": {} }},
            * @OA\RequestBody(
            *    @OA\MediaType(
            *       mediaType="multipart/form-data",
            *       @OA\Schema(
            *           required={"name","price"},
            *           @OA\Property(property="name", type="string", example="Sugar"),
            *           @OA\Property(property="price", type="integer", example="40000"),
            *           @OA\Property(property="photo", type="file"),
            *       )
            *    ),
            * ),
             * @OA\Response(
                  *    response=200,
                  *    description="Success",
                  *    )
              *     )
          *)
       */

    public function create(Request $request) {
        $input = $this->validate($request, [
          'name' => 'required|string',
          'price' => 'required|numeric'
        ]);

        try{
          $user = Auth::user()->user;

          $product = new Product();
          $product->user_id = $user->id;

          if ($request->hasFile('photo')) {
            $filename =  Str::random(32).'.'.$request->photo->extension();
            $path = $request->file('photo')->move('storage/products', $filename);
            $product->photo = 'storage/products'.'/'.$filename;
          } 

          $product->name = $input['name'];
          $product->price = $input['price'];
          $product->save();

          return $this->successResponse($product);

        }catch(\Exception $e){
          return $this->errorResponse($e, 500);
        }
    }

       /**
       * @OA\Post(
            * path="/api/product/{id}",
            * description="Update Product",
            * tags={"Product"},
            * security={ {"bearer": {} }},
            * @OA\Parameter(
            *    in="path",
            *    name="id",
            *    required=true
            * ),
            * @OA\RequestBody(
            *    @OA\MediaType(
            *       mediaType="multipart/form-data",
            *       @OA\Schema(
            *           required={"name","price"},
            *           @OA\Property(property="name", type="string", example="Sugar"),
            *           @OA\Property(property="price", type="integer", example="40000"),
            *           @OA\Property(property="photo", type="file"),
            *       )
            *    ),
            * ),
             * @OA\Response(
                  *    response=200,
                  *    description="Success",
                  *    )
              *     )
          *)
       */


     public function update(Request $request, $id){
      
        $this->validate($request, [
          'name' => 'required|string',
          'price' => 'required|numeric'
        ]);

        try{
          $product = Product::find($id);
          $product->name = $request->name;
          $product->price = $request->price;
          if ($request->hasFile('photo')) {
            unlink($product->photo);
            $filename =  Str::random(32).'.'.$request->photo->extension();
            $path = $request->file('photo')->move('storage/products', $filename);
            $product->photo = 'storage/products'.'/'.$filename;
          }

          $product->save();

          return $this->successResponse($product);
        }catch(\Exception $e) {
            return $this->errorResponse($e, 500);
        }
    }

      /**
       * @OA\Get(
            * path="/api/product",
            * description="Get All Product",
            * tags={"Product"},
            * security={ {"bearer": {} }},
            * @OA\Response(
                  *    response=200,
                  *    description="Success",
                  *    )
              *     )
          *)
       */

    public function get(){
        try {
          $user = Auth::user()->user;

          $product = Product::where(['user_id' => $user->id])->get();
          
          return $this->successResponse($product);

        } catch (\Exception $e) {
           return $this->errorResponse($e, 500);
        }
    }

     /**
       * @OA\Get(
            * path="/api/product/{id}",
            * description="Get Product By Id",
            * tags={"Product"},
            * security={ {"bearer": {} }},
            * @OA\Parameter(
            *    in="path",
            *    name="id",
            *    required=true
            * ),
            * @OA\Response(
                  *    response=200,
                  *    description="Success",
                  *    )
              *     )
          *)
       */

    public function getById($id){
      try {
        $product = Product::find($id);
        return $this->successResponse($product);

      } catch (\Exception $e) {
        return $this->errorResponse($e, 500);
      }
    }

     /**
       * @OA\Delete(
            * path="/api/product/{id}",
            * description="Delete Product",
            * tags={"Product"},
            * security={ {"bearer": {} }},
             * @OA\Parameter(
            *    in="path",
            *    name="id",
            *    required=true
            * ),
            * @OA\Response(
                  *    response=200,
                  *    description="Success",
                  *    )
              *     )
          *)
       */

    public function delete($id){
      try {
        $pos = Product::find($id);
        $pos->delete();

        return $this->successResponse();

      } catch (\Exception $e) {
        return $this->errorResponse($e, 500);
      }
    }

    //
}
