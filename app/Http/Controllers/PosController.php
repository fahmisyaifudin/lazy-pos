<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Model\Pos;

class PosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function create(Request $request) {
        $input = $this->validate($request, [
          'name' => 'required|string',
          'username' => 'required|unique:pos|string|min:6|alpha_num',
          'password' => 'required|string|min:6|alpha_num'
        ]);

        try{
          $user = Auth::user()->user;

          $input['password'] = Hash::make($input['password']);
          $input['user_id'] = $user->id;
          $input['auth_key'] = Str::random(32);

          $pos = Pos::create($input);

          return $this->successResponse($pos);

        }catch(\Exception $e){
          return $this->errorResponse($e, 500);
        }
    }

    public function get(){
        try {
          $user = Auth::user()->user;

          $pos = Pos::where(['user_id' => $user->id])->get();
          
          return $this->successResponse($pos);

        } catch (\Exception $e) {
           return $this->errorResponse($e, 500);
        }
    }

    public function getById($id){
      try {
        $pos = Pos::find($id);
        return $this->successResponse($pos);

      } catch (\Exception $e) {
        return $this->errorResponse($e, 500);
      }
    }

    public function update(Request $request, $id){

      $this->validate($request, [
        'name' => 'required|string'
      ]);

      try{

        $pos = Pos::find($id);
        $pos->name = $request->name;
        $pos->password = $request->password ? Hash::make($request->password) : $pos->password;
        $pos->save();

        return $this->successResponse($pos);
      }catch(\Exception $e) {
          return $this->errorResponse($e, 500);
      }
    }

    public function delete($id){
      try {
        $pos = Pos::find($id);
        $pos->delete();

        return $this->successResponse();

      } catch (\Exception $e) {
        return $this->errorResponse($e, 500);
      }
    }

    //
}
