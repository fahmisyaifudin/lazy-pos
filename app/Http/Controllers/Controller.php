<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

    /**
     * @OA\Info(title="Lazy Pos", version="0.1")
     * @OA\Servers(url="http://localhost:8080")
      * @OA\SecurityScheme(
        *      securityScheme="bearer",
        *      in="header",
        *      name="Authorization",
        *      type="http",
        *      scheme="Bearer",
        *      bearerFormat="JWT",
        * ),
     */

    protected function respondWithToken($token)
    {
        return response()->json([
            'message' => 'success',
            'data' => [
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => Auth::factory()->getTTL() * 60
            ]
        ], 200);
    }

    protected function successResponse($data = null)
    {
        return response()->json([
            'message' => 'success',
            'data' => $data
        ], 200);
    }

    protected function errorResponse($message, $status)
    {
        return response()->json([
            'message' => $message
        ], $status);
    }
}
