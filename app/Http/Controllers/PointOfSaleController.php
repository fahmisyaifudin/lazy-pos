<?php

namespace App\Http\Controllers;

use App\Model\Pos;
use App\Model\Product;
use App\Model\Transaction;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;

class PointOfSaleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    protected function jwt(Pos $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->auth_key, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        try {
            $pos = Pos::where('username', $request->username)->with('user')->first();
            if (!$pos) {
                return $this->errorResponse('Username does not exist', 400);
            }

            if (Hash::check($request->password, $pos->password)) {
                return $this->successResponse(['user' => $pos, 'token' => $this->jwt($pos)]);  
            }
            
            return $this->errorResponse('Password or username wrong', 400);
        } catch (\Exception $e) {
            return $this->errorResponse($e, 500);
        }
    }

    public function getProduct(Request $request)
    {
        try {
            $product = Product::where('user_id', $request->identity->user_id)->get();
            return $this->successResponse($product);
        } catch (\Exception $e) {
            return $this->errorResponse($e, 500);
        }  
    }

    public function createTransaction(Request $request)
    {
        $this->validate($request, [
            'item' => 'required|json'
        ]);

        try {
            $amount = 0;
            $items = json_decode($request->item);
            foreach ($items as $key => $item) {
                $amount += $item->price * $item->quantity;
            }

            $transaction = new Transaction();
            $transaction->pos_id = $request->identity->id;
            $transaction->item = $request->item;
            $transaction->nominal = $amount;
            $transaction->save();

            return $this->successResponse($transaction);
        } catch (\Exception $e) {
            return $this->errorResponse($e, 500);
        }
    }

    //
}
