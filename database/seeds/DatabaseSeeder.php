<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('accounts')->insert(
            [
                [
                    'id' => 1,
                    'email' => 'fahmisyaifudin@mail.com',
                    'password' => Hash::make('password'),
                    'phone' => '08123456789',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 2,
                    'email' => 'alirahmad@mail.com',
                    'password' => Hash::make('password'),
                    'phone' => '08123456788',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 3,
                    'email' => 'mqadri@mail.com',
                    'password' => Hash::make('password'),
                    'phone' => '08123456787',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ]
            ]
        );

        DB::table('users')->insert(
            [
                [
                    'id' => 1,
                    'account_id' => 1,
                    'firstName' => 'Fahmi',
                    'lastName' => 'Syaifudin',
                    'shopName' => 'Toko Elektronik Fahmi',
                    'address' => 'Jln Sesama No 1',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 2,
                    'account_id' => 2,
                    'firstName' => 'Ali',
                    'lastName' => 'Rahmad',
                    'shopName' => 'Toko Buah Bang Ali',
                    'address' => 'Jln Sesama No 2',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 3,
                    'account_id' => 3,
                    'firstName' => 'M',
                    'lastName' => 'Qadri',
                    'shopName' => 'Warkop Cak Qadri',
                    'address' => 'Jln Sesama No 3',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ]
            ]
        );

        DB::table('pos')->insert(
            [
                [
                    'id' => 1,
                    'user_id' => 1,
                    'name' => 'Andi',
                    'username' => 'andi01',
                    'password' => Hash::make('password'),
                    'auth_key' => Str::random(32),
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 2,
                    'user_id' => 2,
                    'name' => 'Joko',
                    'username' => 'joko01',
                    'password' => Hash::make('password'),
                    'auth_key' => Str::random(32),
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 3,
                    'user_id' => 3,
                    'name' => 'Andre',
                    'username' => 'andre01',
                    'password' => Hash::make('password'),
                    'auth_key' => Str::random(32),
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
            ]
        );

        DB::table('products')->insert(
            [
                [
                    'id' => 1,
                    'user_id' => 1,
                    'photo' => 'storage/products/1.jpg',
                    'name' => 'Setrika Philips',
                    'price' => '320000',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 2,
                    'user_id' => 1,
                    'photo' => 'storage/products/2.jpg',
                    'name' => 'Blender Airlux',
                    'price' => '420000',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 3,
                    'user_id' => 2,
                    'photo' => 'storage/products/3.jpg',
                    'name' => 'Tas Wanita',
                    'price' => '120000',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 4,
                    'user_id' => 2,
                    'photo' => 'storage/products/4.jpg',
                    'name' => 'IPhone 8',
                    'price' => '5200000',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 5,
                    'user_id' => 3,
                    'photo' => 'storage/products/5.jpg',
                    'name' => 'Sofa Keluarga Central',
                    'price' => '1200000',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ],
                [
                    'id' => 6,
                    'user_id' => 3,
                    'photo' => 'storage/products/6.jpg',
                    'name' => 'Kursi Kerja Informa',
                    'price' => '650000',
                    'created_at' => date('Y-m-d H:m:s'),
                    'updated_at' => date('Y-m-d H:m:s'),
                ]
            ]
        );
    }
}
