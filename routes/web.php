<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('register', 'AuthController@register');
    $router->get('profile', 'ProfileController@get');
    $router->post('login', 'AuthController@login');
    $router->get('transaction', 'TransactionController@index');

    $router->group(['prefix' => 'pos'], function () use ($router) {
        $router->post('/', 'PosController@create');
        $router->get('/', 'PosController@get');
        $router->get('/{id}', 'PosController@getById');
        $router->post('/{id}', 'PosController@update');
        $router->delete('/{id}', 'PosController@delete');
    });

    $router->group(['prefix' => 'product'], function () use ($router) {
        $router->post('/', 'ProductController@create');
        $router->get('/', 'ProductController@get');
        $router->get('/{id}', 'ProductController@getById');
        $router->post('/{id}', 'ProductController@update');
        $router->delete('/{id}', 'ProductController@delete');
    });

});

$router->group(['prefix' => 'pos'], function () use ($router) {
    $router->post('login', 'PointOfSaleController@login');

    $router->group(['middleware' => 'pos.auth'], function() use ($router) {
        $router->get('/product', 'PointOfSaleController@getProduct');       
        $router->post('/transaction', 'PointOfSaleController@createTransaction');
    });

});
