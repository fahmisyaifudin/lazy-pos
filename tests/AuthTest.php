<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
// use GuzzleHttp\Client;

class AuthTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAuth()
    {
        $register = $this->call('POST', 'api/register', [
            'email' => 'adityafariz76@gmail.com',
            'password' => '123456',
            'firstName' => 'Fariz',
            'lastName' => 'Aditya',
            'shopName' => 'Mendoan',
            'password_confirmation' => '123456',
            'phone' => '0876126512'
        ]);

        $this->assertEquals(200, $register->status());

        $login = $this->call('POST', 'api/login', [
            'email' => 'adityafariz76@gmail.com',
            'password' => '123456',
        ]);

        $this->assertEquals(200, $login->status());
        
        echo 'Auth Success';
    }
}
