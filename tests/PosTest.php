<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
// use GuzzleHttp\Client;

class PosTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCRUDPos()
    {
        $this->call('POST', 'api/register', [
            'email' => 'adityafariz76@gmail.com',
            'password' => '123456',
            'firstName' => 'Fariz',
            'lastName' => 'Aditya',
            'shopName' => 'Mendoan',
            'password_confirmation' => '123456',
            'phone' => '0876126512'
        ]);

        $login = $this->call('POST', 'api/login', [
            'email' => 'adityafariz76@gmail.com',
            'password' => '123456',
        ]);

        $token = json_decode($login->getContent());

        $createPos = $this->call('POST', 'api/pos', [
            'name' => 'anwar',
            'username' => 'anwar1233',
            'password' => '1234qw'
        ], ['Authorization' => 'Bearer '.$token->token]);

        $this->assertEquals(200, $createPos->status());

        $posRes = json_decode($createPos->getContent());

        echo "Create Pos Success \n";

        $updatePos = $this->call('POST', 'api/pos/'.$posRes->data->id, [
            'name' => 'anwar',
            'password' => '1234qw'
        ], ['Authorization' => 'Bearer '.$token->token]);

        $this->assertEquals(200, $updatePos->status());

        echo "Update Pos Success \n";

        $getPos = $this->call('GET', 'api/pos', [], ['Authorization' => 'Bearer '.$token->token]);

        $this->assertEquals(200, $getPos->status());

        echo "Get All Pos Success \n";

        $detailPos = $this->call('GET', 'api/pos/'.$posRes->data->id, [], ['Authorization' => 'Bearer '.$token->token]);

        $this->assertEquals(200, $detailPos->status());

        echo "Get Detail Pos Success \n";

        $deletePos = $this->call('DELETE', 'api/pos/'.$posRes->data->id, [], ['Authorization' => 'Bearer '.$token->token]);

        $this->assertEquals(200, $deletePos->status());

        echo "Delete Pos Success \n";

    }
}
